# Changelog

All notable changes to this project will be documented in this file.

The format is loosely based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this
project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.htm)l.

## Release 0.2.1

Final release, 2024-08-31.

* This project is discontinued.
  I recommend <https://github.com/ducaale/xh> instead.
* Updated `reqwest` to 0.12.7.

## Release 0.2.0

Initial release, 2022-02-06.

* Support GET, HEAD, PATCH, POST, PUT, and DELETE requests.
* Writes response body to stdout.
* Arguments for generic headers, bearer authentication, request body
  and content type.
* A `--verbose` flag to show request and response headers on stderr.
