# url

Convenient command line interface to urls.

## Project discontinued

Since writing this, I have found [`xh`](https://github.com/ducaale/xh)
which is basically the same thing but way more feature-complete.
Therefore, I discontinue this project and recommend the use of `xh`
instead.


## Old introduction

There are subcommands for different http methods (verbs).
If there is a response body, it will be written to standard output.
There is a `--verbose` option to get the request and response headers
written to standard error, and various options for creating the request.
If standard out is a terminal, text output will be decoded and written
as utf-8.
If standard out is not a terminal, the exact bytes of the response
body will be written as is.

## Contributing

The url command line is open source.
If you find a problem, please report an issue.
If you can and want to, you may even get the code, change it to fix
the problem and submit your changes as a pull request, so it can be
included in the next release.
