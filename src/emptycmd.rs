use crate::client;
use crate::common::{HeaderArgs, RequestBuilderExt};
use anyhow::Result;
use reqwest::Method;

/// Subcommand for a request that does not have a body.
#[derive(clap::Parser)]
pub struct EmptyCmd {
    #[command(flatten)]
    client: client::Args,
    #[command(flatten)]
    headers: HeaderArgs,

    /// The url to request.
    url: String,
}

impl EmptyCmd {
    pub fn run(self, method: Method) -> Result<()> {
        let client = self.client.build()?;
        let request = client
            .request(method, &self.url)
            .header_args(self.headers)?
            .build()?;
        client.execute(request)
    }
}
