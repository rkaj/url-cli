use crate::client;
use crate::common::{BodyArgs, HeaderArgs, RequestBuilderExt};
use anyhow::Result;
use reqwest::Method;

/// Subcommand for a request that does not have a body.
#[derive(clap::Parser)]
pub struct BodyCmd {
    #[command(flatten)]
    client: client::Args,
    #[command(flatten)]
    headers: HeaderArgs,
    #[command(flatten)]
    body: BodyArgs,

    /// The url to request.
    url: String,
}

impl BodyCmd {
    pub fn run(self, method: Method) -> Result<()> {
        let client = self.client.build()?;
        let request = client
            .request(method, &self.url)
            .header_args(self.headers)?
            .body_args(self.body)?
            .build()?;
        client.execute(request)
    }
}
