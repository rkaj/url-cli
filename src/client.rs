use crate::common::{RequestExt, ResponseExt};
use anyhow::{ensure, Result};
use is_terminal::IsTerminal;
use reqwest::blocking::{Request, RequestBuilder};
use reqwest::Method;
use std::io::{stdout, Write};

#[derive(clap::Args)]
pub struct Args {
    /// Log the request and response headers to stderr.
    #[arg(long, short)]
    verbose: bool,

    /// Tell the server what user agent is doing the request.
    #[arg(long, default_value=concat!(
        env!("CARGO_PKG_NAME"),
        "/",
        env!("CARGO_PKG_VERSION"),
        " ",
        env!("CARGO_PKG_REPOSITORY"),
    ))]
    user_agent: String,
}

impl Args {
    pub fn build(self) -> Result<Client> {
        Ok(Client {
            client: reqwest::blocking::Client::builder()
                .user_agent(self.user_agent)
                .build()?,
            verbose: self.verbose,
        })
    }
}
pub struct Client {
    client: reqwest::blocking::Client,
    verbose: bool,
}

impl Client {
    pub fn request(&self, method: Method, url: &str) -> RequestBuilder {
        self.client.request(method, url)
    }
    pub fn execute(&self, request: Request) -> Result<()> {
        let response = self
            .client
            .execute(request.show(self.verbose))?
            .show(self.verbose)
            .error_for_status()?;
        let out = stdout();
        if out.is_terminal() {
            let text = response.text()?;
            ensure!(
                !text.contains(char::REPLACEMENT_CHARACTER),
                "Not writing non-text response to terminal."
            );
            stdout().write_all(text.as_ref())?;
        } else {
            stdout().write_all(&response.bytes()?)?;
        }
        Ok(())
    }
}
