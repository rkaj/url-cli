mod bodycmd;
mod client;
mod common;
mod emptycmd;

use anyhow::Result;
use clap::Parser;
use reqwest::Method;

/// Convenient command line interface to urls.
///
/// There are subcommands for different http methods (verbs).
/// If there is a response body, it will be written to standard output.
/// There is a `--verbose` option to get the request and response
/// headers written to standard error, and various options for
/// creating the response.
/// If standard out is a terminal, text output will be decoded and
/// written as utf-8.
/// If standard out is not a terminal, the exact bytes of the response
/// body will be written as is.
///
/// See the help for each subcommand for details.
#[derive(Parser)]
#[command(
    about,
    author,
    version,
    after_help = "\
    The code is on https://codeberg.org/rkaj/url-cli.\
    \n\n\
    Please note: This project is discontinued.  \
    I recommend https://github.com/ducaale/xh instead."
)]
enum Url {
    /// Do a DELETE request.
    Delete(emptycmd::EmptyCmd),
    /// Do a GET request.
    Get(emptycmd::EmptyCmd),
    /// Do a HEAD request.
    Head(emptycmd::EmptyCmd),
    /// Do a PATCH request.
    Patch(bodycmd::BodyCmd),
    /// Do a POST request.
    Post(bodycmd::BodyCmd),
    /// Do a PUT request
    Put(bodycmd::BodyCmd),
}

fn main() -> Result<()> {
    Url::parse().run()
}

impl Url {
    fn run(self) -> Result<()> {
        match self {
            Url::Delete(cmd) => cmd.run(Method::DELETE),
            Url::Get(cmd) => cmd.run(Method::GET),
            Url::Head(cmd) => cmd.run(Method::HEAD),
            Url::Patch(cmd) => cmd.run(Method::PATCH),
            Url::Post(cmd) => cmd.run(Method::POST),
            Url::Put(cmd) => cmd.run(Method::PUT),
        }
    }
}
