use anyhow::{anyhow, Context, Error, Result};
use reqwest::blocking::{Request, RequestBuilder, Response};
use reqwest::header::{HeaderName, HeaderValue, CONTENT_TYPE};
use std::env;
use std::io::{stdin, Read};
use std::str::FromStr;

/// Common args about request headers.
#[derive(clap::Args)]
pub struct HeaderArgs {
    /// Extra http header to include in the request.
    ///
    /// You may specify any number of extra headers.
    #[arg(long, short = 'H')]
    header: Vec<Header>,

    /// Enable HTTP bearer authentication
    ///
    /// The token is given directly on the command line.
    /// This has security implications, as other users may be able to
    /// read the command line from process listings.
    #[arg(long, value_name = "TOKEN")]
    bearer_auth: Option<String>,

    /// Enable HTTP bearer authentication
    ///
    /// The token is read from environment `$VARNAME`.
    #[arg(long, value_name = "VARNAME")]
    bearer_auth_env: Option<String>,
}

#[derive(clap::Args)]
pub struct BodyArgs {
    /// Json data to post.
    ///
    /// When this argument is used, `--content_type` defaults to
    /// `application/json`, but can still be given to use a more
    /// specific content type, like `application/vnd.my.api-v1+json`.
    #[arg(long)]
    json_body: Option<String>,

    /// The content type of the request body.
    #[arg(long)]
    content_type: Option<String>,

    /// Where to read the request body from.
    ///
    /// The special name `-` implies standard input.
    #[arg(long)]
    body_from: Option<String>,

    /// The request body
    #[arg(long, required_unless_present_any(["json_body", "body_from"]))]
    body: Option<String>,
}

pub trait RequestExt {
    fn show(self, verbose: bool) -> Self;
}
pub trait RequestBuilderExt {
    type Result; // Workaround; Result<Self> can't be used directly, size now known.
    fn header_args(self, args: HeaderArgs) -> Self::Result;
    fn body_args(self, args: BodyArgs) -> Self::Result;
}

pub trait ResponseExt {
    fn show(self, verbose: bool) -> Self;
}

impl RequestExt for Request {
    fn show(self, verbose: bool) -> Self {
        if verbose {
            eprintln!("> {} {} {:?}", self.method(), self.url(), self.version());
            for (name, value) in self.headers() {
                eprintln!("> {name}: {value:?}");
            }
        }
        self
    }
}
impl RequestBuilderExt for RequestBuilder {
    type Result = Result<Self>;

    fn header_args(self, args: HeaderArgs) -> Self::Result {
        let t = self.headers(args.header.into_iter().map(Into::into).collect());
        Ok(if let Some(var) = args.bearer_auth_env {
            t.bearer_auth(env::var(&var).map_err(|e| anyhow!("Bearer auth: {var:?} {e}"))?)
        } else if let Some(token) = args.bearer_auth {
            t.bearer_auth(token)
        } else {
            t
        })
    }
    fn body_args(self, args: BodyArgs) -> Self::Result {
        Ok(if let Some(json_body) = args.json_body {
            self.header(
                CONTENT_TYPE,
                args.content_type.as_deref().unwrap_or("application/json"),
            )
            .body(json_body)
        } else {
            let request = if let Some(content_type) = args.content_type {
                self.header(CONTENT_TYPE, content_type)
            } else {
                self
            };
            if let Some(name) = args.body_from {
                request.body(if name == "-" {
                    let mut buf = Vec::new();
                    stdin().read_to_end(&mut buf)?;
                    buf
                } else {
                    std::fs::read(name)?
                })
            } else if let Some(body) = args.body {
                request.body(body)
            } else {
                unreachable!("Some kind of body must be given")
            }
        })
    }
}

impl ResponseExt for Response {
    fn show(self, verbose: bool) -> Self {
        if verbose {
            let status = self.status();
            eprintln!(
                "< {:?} {status:?} {}",
                self.version(),
                status.canonical_reason().unwrap_or("")
            );
            for (name, value) in self.headers() {
                eprintln!("< {name}: {value:?}");
            }
        }
        self
    }
}

#[derive(Clone, Debug)]
pub struct Header {
    name: HeaderName,
    value: HeaderValue,
}

impl FromStr for Header {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (name, value) = s.split_once(':').context("Expected `:` in header")?;
        Ok(Header {
            name: name.trim().parse()?,
            value: value.trim().parse()?,
        })
    }
}

impl From<Header> for (HeaderName, HeaderValue) {
    fn from(value: Header) -> Self {
        (value.name, value.value)
    }
}
